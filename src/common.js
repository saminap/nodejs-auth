module.exports = {
  SECRET: 'supersecretkeythatnobodyknows',
  SALTROUNDS: 12,
  LOGINTOKENLIFETIME: { minutes: 5 },
}
