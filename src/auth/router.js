const express = require('express');
const router = express.Router();
const methods = require('./methods');

router.post('/login', methods.login);
router.post('/logout', methods.logout);
router.post('/register', methods.register);

module.exports = router;
