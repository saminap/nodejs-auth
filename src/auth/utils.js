const jwt = require('jsonwebtoken');
const moment = require('moment');
const common = require('./../common');
const User = require('./../users');

module.exports = {

generateLoginToken: (user) => {
  return jwt.sign({
      username: user.username,
      id: user.id,
    }, common.SECRET)
},

generateLoginIncorrectMsg: () => {
  return {
    message: "Wrong username and/or password"
  }
},

generateLoginFirstMsg: () => {
  return {
    message: "Token invalid. Please login first"
  }
},

generateLoginCookie: (res, token) => {
  res.cookie('token', token, {httpOnly: true});
  return res;
},

validate: (req, res, next) => {
  if(req.cookies && req.cookies.token) {
    const token = req.cookies.token;

    jwt.verify(token, common.SECRET, (err, decoded) => {
      if(err) {
        res.status(401).send(module.exports.generateLoginFirstMsg());
        return;
      }

      const now = moment(new Date());
      const expiration = moment.unix(decoded.iat).add(common.LOGINTOKENLIFETIME);

      if(expiration > now)
        next();
      else {
        User.findOne({where: {id: decoded.id}})
        .then(user => {
          if(!user.allowLogin) {
            res.sendStatus(403);
            return;
          }
          else {
            module.exports.generateLoginCookie(
              res,
              module.exports.generateLoginToken(user)
            );
            next();
          }
        });
      }
    });
  }
  else {
    res.status(401).send(module.exports.generateLoginFirstMsg());
  }
},


}
