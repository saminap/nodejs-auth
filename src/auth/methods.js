const User = require('./../users');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const utils = require('./utils');
const bcrypt = require('bcrypt');
const common = require('./../common');

module.exports = {

login: (req, res) => {
  const { username, password } = req.body;

  if(typeof username === 'undefined' ||
     typeof password === 'undefined') {
    res.sendStatus(400);
    return;
  }

  User.findOne({where: {username: username} })
    .then(user => {
      if(user !== null) {
        bcrypt.compare(password, user.password)
          .then(result => {
            if(result)
            {
              if(user.allowLogin) {
                utils.generateLoginCookie(
                  res,
                  utils.generateLoginToken(user))
                .sendStatus(200);
              }
              else
                res.sendStatus(403);
            }
            else
              res.status(401).send(utils.generateLoginIncorrectMsg());
          })
          .catch(err => {
            res.status(401).send(utils.generateLoginIncorrectMsg());
          });
      }
      else {
        res.status(401).send(utils.generateLoginIncorrectMsg());
      }
    })
    .catch(err => {
      res.sendStatus(400);
      throw err;
    });
},

logout: (req, res) => {
  res.clearCookie('token').sendStatus(200);
},

register: (req, res) => {
  const { username, password } = req.body;
  const hash = bcrypt.hashSync(password, common.SALTROUNDS);

  User.create({ username: username, password: hash})
    .then(user => {
      res.sendStatus(201);
    })
    .catch(err => {
      res.sendStatus(400);
      throw err;
    });
}

}
