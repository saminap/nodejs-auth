const Sequelize = require('sequelize');
const sequelize = new Sequelize('postgres://postgres:postgres@localhost:5432/sami');
const uuid = require('uuid/v4');

class User extends Sequelize.Model {}
User.init({
  username: {
    type: Sequelize.STRING,
  },

  password: {
    type: Sequelize.STRING,
  },

  allowLogin: {
    type: Sequelize.BOOLEAN,
  }

}, { sequelize, modelName: 'user' });

User.beforeCreate((user, _ ) => {
  return user.id = uuid();
});

module.exports = User;
