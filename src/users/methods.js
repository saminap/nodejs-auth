const User = require('./../users');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const bcrypt = require('bcrypt');
const common = require('./../common');

module.exports = {

showAll: (req, res) => {
  User.findAll().then(users => {
    users = users.map(user => user.username);
    res.send(users);
  });
}

}
