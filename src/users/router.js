const express = require('express');
const router = express.Router();
const methods = require('./methods');
const auth = require('./../auth/utils');

router.get('/', auth.validate, methods.showAll);

module.exports = router;
