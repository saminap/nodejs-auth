const express = require('express');
const app = express();
const router = express.Router();
const authRouter = require('./auth/router');
const usersRouter = require('./users/router');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(bodyParser.json());

router.get('/', (req, res) => res.sendStatus(200));
router.use('/auth', authRouter);
router.use('/users', usersRouter);

app.use('/api/v1', router);

const PORT = process.env.PORT || 4000;
app.listen(PORT, console.log(`listening at ${PORT}`));
